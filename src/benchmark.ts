import Benchmark from 'benchmark';
import { TypescriptSafeCast, safeCast, SchemaSafeCast } from '.';
import { TestType } from './__tests__/examples/simple-type';

function createTypescriptSafeCast(): TypescriptSafeCast {
  return new TypescriptSafeCast({
    importFrom: {
      type: 'ts-files',
      tsFiles: ['src/__tests__/examples/simple-type/index.ts']
    }
  });
}

let safeCaster = createTypescriptSafeCast();

function createSchemaSafeCaster(): SchemaSafeCast {
  return new SchemaSafeCast({
    type: 'object',
    properties: {
      id: { type: 'number' },
      name: { type: 'string' },
      values: {
        type: 'object',
        required: [ 'val1' ],
        properties: {
          val1: { type: 'string' },
          val2: { type: 'string' }
        }
      }
    },
    required: [ 'id', 'name', 'values' ]
  });
}

let schemaSafeCaster = createSchemaSafeCaster();

function printResult(benchmark: Benchmark) {
  console.log('## ' + (benchmark as any).name);
  console.log();
  const stats = benchmark.stats;
  console.log(`mean ${stats.mean} - deviation ${stats.deviation}`);
  console.log(`variance ${stats.variance}`);
  console.log(`Margin of Error ${stats.moe}`);
  console.log(`Relative Margin of Error ${stats.rme}`);
  console.log();
}

const suite = new Benchmark.Suite('Basic Cast', {
  onComplete: (a: any, b: any) => {
    a.currentTarget.forEach((benchmark: any) => {
      printResult(benchmark);
    });
  },
  minSamples: 10000
}).add('Slow Typescript Cast', () => {
  createTypescriptSafeCast().cast<TestType>("TestType", {
    id: 1234,
    name: 'hi',
    values: {
      val1: 'woo'
    }
  });
}).add('Fast Typescript Cast', () => {
  safeCaster.cast<TestType>("TestType", {
    id: 1234,
    name: 'hi',
    values: {
      val1: 'woo'
    }
  });
}).add('Simple Typescript Cast', () => {
  safeCast<TestType>("TestType", ['src/__tests__/examples/simple-type/index.ts'], {
    id: 1234,
    name: 'hi',
    values: {
      val1: 'woo'
    }
  });
}).add('Slow Schema Cast', () => {
  const caster = createSchemaSafeCaster();

  caster.cast<TestType>({
    id: 1234,
    name: 'hi',
    values: {
      val1: 'woo'
    }
  });
}).add('Fast Schema Cast', () => {
  schemaSafeCaster.cast<TestType>({
    id: 1234,
    name: 'hi',
    values: {
      val1: 'woo'
    }
  });
});

suite.run();