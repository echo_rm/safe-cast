import { CastError } from './safe-cast';
import { TypescriptSafeCast } from './typescript-safe-cast';

class CacheingTypescriptSafeCast {
  private casterLookup: Map<string, TypescriptSafeCast>;

  constructor() {
    this.casterLookup = new Map<string, TypescriptSafeCast>();
  }

  private static lookupKey(uniqueFilenames: string[]): string {
    return uniqueFilenames.sort().join('|');
  }

  public cast<Type>(typeClassName: string, tsFiles: string[], data: any): Type | CastError {
    const uniqueFilenames = [...new Set(tsFiles)];
    const lookupKey = CacheingTypescriptSafeCast.lookupKey(uniqueFilenames);

    let caster = this.casterLookup.get(lookupKey);
    if (caster === undefined) {
      caster = new TypescriptSafeCast({
        importFrom: {
          type: 'ts-files',
          tsFiles: uniqueFilenames
        }
      });
      this.casterLookup.set(lookupKey, caster);
    }

    return caster.cast<Type>(typeClassName, data);
  }
}

const singletonCachingCaster = new CacheingTypescriptSafeCast();

export function safeCast<Type>(typeClassName: string, tsFiles: string[], data: any): Type | CastError {
  return singletonCachingCaster.cast<Type>(typeClassName, tsFiles, data);
}