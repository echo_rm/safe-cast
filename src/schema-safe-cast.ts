import { CastError, SafeCast } from "./safe-cast";
import Ajv from 'ajv';

export class SchemaSafeCast {
  private validate: Ajv.ValidateFunction;

  constructor(jsonSchema: any) {
    const ajv = new Ajv({
      schemaId: 'auto',
      validateSchema: true
    });
    ajv.addMetaSchema(require('ajv/lib/refs/json-schema-draft-04.json'));
    this.validate = ajv.compile(jsonSchema);
  }

  public cast<Type>(data: any): Type | CastError {
    const valid = this.validate(data);
    if (valid !== true) {
      const error = this.validate.errors === undefined || this.validate.errors === null
        ? 'Could not validate the types succefully despite no errors present'
        : this.validate.errors.map(error => error.message).join('\n');
      return new CastError(error);
    }

    return data;
  }
}

type SchemaMap = { [typeClassName: string]: any };

/**
 * We should be able to be passed in an object with a bunch of schemas and then auto-cast to whatever we want.
 */
class SchemaLoadingSafeCast implements SafeCast {
  private casters: { [typeClassName: string]: SchemaSafeCast };

  constructor(private _schemas: SchemaMap) {
    this.casters = {};
  }

  public cast<Type>(typeClassName: string, data: any): Type | CastError {
    let caster = this.casters[typeClassName];
    if (caster === undefined) {
      const schema = this._schemas[typeClassName];
      if (schema === undefined) {
        return new CastError(`Could not find a schema for the Type Class: ${typeClassName}`);
      }
      caster = new SchemaSafeCast(schema);
      this.casters[typeClassName] = caster;
    }

    return caster.cast<Type>(data);
  }
}

export function loadSchemas(schemas: SchemaMap): SafeCast {
  return new SchemaLoadingSafeCast(schemas);
}