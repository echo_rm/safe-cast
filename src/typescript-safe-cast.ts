import * as TJS from 'typescript-json-schema';
import Ajv from 'ajv';
import { CastError, SafeCast } from './safe-cast';

export type SafeCastOptions = {
  importFrom: ImportFrom;
};

export type ImportFrom = ImportFromProgramFiles | ImportFromTSConfig;

export type ImportFromProgramFiles = {
  type: 'ts-files';
  tsFiles: string[];
};

export type ImportFromTSConfig = {
  type: 'tsconfig';
  tsConfigFilePath: string;
};

export class TypescriptSafeCast implements SafeCast {
  private generator: TJS.JsonSchemaGenerator;

  constructor(options: SafeCastOptions) {
    const { importFrom } = options;
    const program = TypescriptSafeCast.loadProgramFrom(importFrom);

    // TODO should we allow people to give us these arguments?
    const settings: TJS.PartialArgs = {
      noExtraProps: true,
      strictNullChecks: true,
      required: true,
      uniqueNames: true
    };

    const generator = TJS.buildGenerator(program, settings);
    if (generator === null) {
      throw new Error('Could not build a SafeCast environment for the provided options.');
    }
    this.generator = generator;
  }

  private static loadProgramFrom(importFrom: ImportFrom): TJS.Program {
    if (importFrom.type === 'tsconfig') {
      return TJS.programFromConfig(importFrom.tsConfigFilePath);
    } else {
      // console.log(`TS files: ${importFrom.tsFiles}`);
      return TJS.getProgramFromFiles(importFrom.tsFiles);
    }
  }

  public cast<Type>(typeClassName: string, data: any): Type | CastError {
    // Get the type that I am supposed to cast to
    // Note: It turns out that this is impossible to do nicely, using typeClassName instead as a passed in argument.

    // Pass it to typescript json schema and generate a schema

    const schema = this.getSchemaForSymbol(typeClassName);
    if (schema instanceof CastError) {
      return schema;
    }

    // If you want to use both draft-04 and draft-06/07 schemas:
    const ajv = new Ajv({schemaId: 'auto'});
    ajv.addMetaSchema(require('ajv/lib/refs/json-schema-draft-04.json'));
    const validate = ajv.compile(schema);

    const valid = validate(data);
    if (valid !== true) {
      const error = validate.errors === undefined || validate.errors === null
        ? 'Could not validate the types succefully despite no errors present'
        : validate.errors.map(error => error.message).join('\n');
      return new CastError(error);
    }

    return data;
  }

  private getSchemaForSymbol(typeClassName: string): TJS.Definition | CastError {
    try {
      const symbol = this.generator.getSymbols().find(s => s.typeName === typeClassName);
      if (symbol === undefined) {
        return new CastError(`Could not find a type with the name: ${typeClassName}`);
      }
      return this.generator.getSchemaForSymbol(symbol.name); // Note: This will throw a runtime error if lookup fails
    } catch (e) {
      return new CastError(e.message, e.stack);
    }
  }
}