import { SchemaSafeCast } from "../schema-safe-cast";
import { CastError } from '../safe-cast';

/**
 * @jest-environment node
 */
describe('TypescriptSafeCast', () => {
  it('should validate against a simple JSON schema', () => {
    type SimpleData = {
      id: number;
      name: string;
    };

    const caster = new SchemaSafeCast({
      type: 'object',
      properties: {
        id: { type: 'number' },
        name: { type: 'string', 'minLength': 4 }
      }
    });

    const input = {
      id: 1234,
      name: 'test-input'
    };

    const result = caster.cast<SimpleData>(input);

    expect(result).toEqual(input);
  });

  it('should result in an error against a simple JSON schema if invalid', () => {
    type SimpleData = {
      id: number;
      name: string;
    };

    const caster = new SchemaSafeCast({
      type: 'object',
      properties: {
        id: { type: 'number' },
        name: { type: 'string', 'minLength': 4 }
      }
    });

    const input = {
      id: 1234,
      name: 'hi'
    };

    const result = caster.cast<SimpleData>(input);

    expect(result).toBeInstanceOf(CastError);
  });
});