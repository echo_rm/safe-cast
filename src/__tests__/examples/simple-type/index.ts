export type TestType = {
  /**
   * @minimum 0
   * @TJS-type integer
   */
  id: number;

  /**
   * @maxLength 50
   */
  name: string;

  values: {
    val1: string,
    val2?: string
  }
}