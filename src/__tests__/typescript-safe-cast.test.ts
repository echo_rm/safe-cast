/**
 * @jest-environment node
 */

import { TypescriptSafeCast } from '../typescript-safe-cast';
import { TestType } from './examples/simple-type';
import { CastError } from '../safe-cast';

describe('TypescriptSafeCast', () => {
  const safeCaster = new TypescriptSafeCast({
    /*
    importFrom: {
      type: 'tsconfig',
      tsConfigFilePath: 'src/__tests__/examples/simple-type/tsconfig.json'
    }
    */
    importFrom: {
      type: 'ts-files',
      tsFiles: ['src/__tests__/examples/simple-type/index.ts']
    }
  });

  it('should correctly validate a file and fail if the type is wrong', () => {
    const result = safeCaster.cast<TestType>("TestType", {
      id: 1234,
      name: 'hi'
    });

    expect(result).toBeInstanceOf(CastError);
  });

  it('should correctly validate a file and succeed if the type is good', () => {
    const result = safeCaster.cast<TestType>("TestType", {
      id: 1234,
      name: 'hi',
      values: {
        val1: 'woo'
      }
    });

    expect(result).not.toBeInstanceOf(CastError);
  });

  it('should not allow additional properties', () => {
    const result = safeCaster.cast<TestType>("TestType", {
      id: 1234,
      name: 'hi',
      values: {
        val1: 'woo'
      },
      additionalProperty: 'hello'
    });

    expect(result).toEqual(new CastError('should NOT have additional properties'));
  });
});