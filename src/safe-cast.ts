export interface SafeCast {
  cast<Type>(typeClassName: string, data: any): Type | CastError;
}

export class CastError {
  constructor(private _message: string, private _stack?: string) {}

  public message(): string {
    return this._message;
  }

  public stack(): string | undefined {
    return this._stack;
  }
}